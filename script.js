/*
var sp = getSpotifyApi(1);
var m = sp.require("sp://import/scripts/api/models");
var v = sp.require("sp://import/scripts/api/views");
var ui = sp.require('sp://import/scripts/dnd');
*/
var socket;
var json = JSON.stringify;


function doConnect(){
    socket = io.connect('http://192.168.1.66:8000');
    socket.on("connect",onConnect);
    socket.on("message",onMessage);
    socket.on('disconnect',onDisconnect);
    socket.on('connect_failed',onConnectFailed);
}

function onDisconnect() {
    console.log('reconnecting...')
    socket.connect();
}

function onConnectFailed() {
    console.log('connection failed. reconnecting...')
    socket.connect()
}
function onConnect(evt){

	console.log(evt);
}

function onMessage(data){
    console.log(data);
    $("#textWindow").append("<li>"+ data.message +"</li>");

}



$(document).ready(function(){

	$(".connect").click(function(){
		doConnect();
	});

    $(".joinBtn").click(function(){
        socket.emit("join",{"channel":$("#channelInput").val()});
    });

    $(".setNicknameBtn").click(function(){
        socket.emit("nickname",{"nickname":$("#nicknameInput").val()});
    });

	$(".sendBtn").click(function(){
        socket.emit("message",{"message":$("#textInput").val()});
	});
});
